import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../user';
import { UserService } from '../user.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  id: any;
  user: User = new User();
  form!: FormGroup ;


  constructor(private userService: UserService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'firstName': new FormControl(null, {
        validators:[
          Validators.required,
          Validators.minLength(3),
          Validators.pattern('[a-zA-Z ]*')]}),

      'lastName' : new FormControl(null, {
        validators:[
          Validators.required,
          Validators.minLength(3),
          Validators.pattern('[a-zA-Z ]*')
        ]
      }),

      'email' : new FormControl(null, {
        validators:[
          Validators.required,
          Validators.email
        ]
      })
    });

    this.id = this.route.snapshot.params['id'];
    this.userService.getUserById(this.id).subscribe( data => {
      this.user = data;
      this.form.setValue({
        firstName: this.user.firstName,
        lastName: this.user.lastName,
        email: this.user.email
      });
    }, error => console.log(error));
  }

  onSubmit() {
    if(this.form.invalid)
    {
      return;
    } else {
    this.userService.updateUser(this.id, this.form.getRawValue()).subscribe( data => {
      this.goToUserList();
    }
    , error => console.log(error));
  }
  }


  goToUserList() {
    this.router.navigate(['/users']);
  }

}
