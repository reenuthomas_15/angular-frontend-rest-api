import { Component, OnInit } from '@angular/core';
// import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../user';
import { UserService } from '../user.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  user: User = new User();
  form!: FormGroup ;

  constructor(private userService: UserService,
    private router: Router) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'firstName': new FormControl(null, {
        validators:[
          Validators.required,
          Validators.minLength(3),
          Validators.pattern('[a-zA-Z ]*')]}),

      'lastName' : new FormControl(null, {
        validators:[
          Validators.required,
          Validators.minLength(3),
          Validators.pattern('[a-zA-Z ]*')
        ]
      }),

      'email' : new FormControl(null, {
        validators:[
          Validators.required,
          Validators.email
        ]
      })
    });


  }

  saveUser() {

    this.userService.createUser(this.form.getRawValue()).subscribe( data => {
      console.log(data);
      this.goToUserList();
    },
    error => console.log(error));
  }

  goToUserList() {
    this.router.navigate(['/users']);
  }

  onSubmit() {
    if(this.form.invalid){
      return;
    } else {
      console.log(this.form);
      this.saveUser();
    }


  }

}
